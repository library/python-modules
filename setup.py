import setuptools

setuptools.setup(
    name='PythonModules',
    version='2.0.8',
    licence='WUR proprietary',
    author="Wageningen UR",
    author_email="mdt-library@wur.nl",
    description="Utilities for WUR Python scrips",    
    url="https://git.wur.nl/library/python-modules",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "lxml",
        "requests",
        "tenacity"
    ]
)
