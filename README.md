# Wageningen UR
## Python utilities library

Wijzigingen:
===========
Als deze library gewijzigd wordt
- verhoog het versienummer in setup.py
- add/commit/push

In een nieuw project de library toevoegen:
==========================================
```python
pipenv install git+ssh://git@git.wur.nl/library/python-modules.git#egg=PythonModules
```

Update een nieuwe versie in je project: 
=======================================
```python
pipenv update
```

Specifieke versie van library instellen:
==========================================
Pas de module in je pipfile aan:
```python
pythonmodules = {git = "ssh://git@git.wur.nl/library/python-modules.git", editable = true, ref = "v1.1.0"}
```

Voorbeeld van library toepassen in de code:
===========================================
```python
from WurModules.util.getConfig import GetConfig
from WurModules.webquery.webquery import WQReceiver

config = GetConfig("./etc/pure-conversie.conf", CONFIG_ENV)

wqReceiver = WQReceiver()
wqReceiver.dryrun = True
wqReceiver.service = 'edepot'
```

