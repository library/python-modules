#!/usr/bin/env python3

import urllib.parse
import urllib.request
import requests
import lxml.etree as ET
import logging
from WurModules.util.sendRequest import send_request

class WQReceiver:
    """This class is used to post/search xml content to/from WebQuery

    When instancing the module, the following parameters can be passed:

    Parameters
    ----------
    service : str
        what service webquery is using (xml for example)
    suffix : str
        suffix of service
    dryrun : Boolean
        True if it is a dryrun, False if not
    logger : class 'logging.Logger'
        logger inherited from main
    isn : int
        isn value, only relevant for updates
    crc : int
        crc value, only relevant for updates
    query : str
    What is the query of your search
    wq_ofs : int
        wq_ofs value
    wq_max : int
        wq_max value
    timeout : int
        timeout value for requests methods

    Methods
    -------
    search()
        based on host, service, suffix, wq_ofs, wq_max, query and isn
        search request with set parameters. stops when wq_max is reached
    search_all_records()
        based on host, service, suffix, wq_ofs, wq_max, query and isn
        run a search request with set request parameters.
        continue searches and collect all records until no next element is found
    get_update_url()
        based on host, service, suffix, isn and crc, returns a string with the url required for update entry request
    get_new_url()
        based on host, service and suffix, returns a string with the url required for new entry request
    create_kv_pairs(xml:str)
        parses xml string and converts it into a series of key-value pairs
        returns result : str
    post_xml(url: str, body: str, method: str = 'post')
        sends provided body to provided url using optionally provided method
        returns response.status_code : str
        if dryrun is true, the request is printed instead of sending it to the server
    """

    def __init__(self,
                 host='https://library.wur.nl',
                 service=None,
                 suffix='xml',
                 dryrun=False,
                 logger=logging.getLogger(__name__),
                 isn=None,
                 crc=None,
                 query=None,
                 wq_ofs=None,
                 wq_max=None,
                 data_response=None,
                 timeout=60):

        self._host = host
        self._service = service
        self._suffix = suffix
        self._dryrun = dryrun
        self._logger = logger
        self._isn = isn
        self._crc = crc
        self._query = query
        self._wq_ofs = wq_ofs
        self._wq_max = wq_max
        self._data_response = data_response
        self._timeout = timeout
        self._response = None
        self._iteration_buffer = []

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, host_input):
        self._host = host_input

    @property
    def service(self):
        return self._service

    @service.setter
    def service(self, service_input):
        self._service = service_input

    @property
    def suffix(self):
        return self._suffix

    @suffix.setter
    def suffix(self, suffix_input):
        self._suffix = suffix_input

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, logger_input):
        self._logger = logger_input

    @property
    def isn(self):
        return self._isn

    @isn.setter
    def isn(self, isn_input):
        self._isn = isn_input

    @property
    def crc(self):
        return self._crc

    @crc.setter
    def crc(self, crc_input):
        self._crc = crc_input

    @property
    def query(self):
        return self._query

    @query.setter
    def query(self, query_input):
        self._query = query_input

    @property
    def data_response(self):
        return self._data_response

    @data_response.setter
    def data_response(self, data_response_input):
        self._data_response = data_response_input

    @property
    def wq_ofs(self):
        return self._wq_ofs

    @wq_ofs.setter
    def wq_ofs(self, wq_ofs_input):
        self._wq_ofs = wq_ofs_input

    @property
    def wq_max(self):
        return self._wq_max

    @wq_max.setter
    def wq_max(self, wq_max_input):
        self._wq_max = wq_max_input

    @property
    def timeout(self):
        return self._timeout

    @timeout.setter
    def timeout(self, timeout_input):
        self._timeout = timeout_input

    @property
    def dryrun(self):
        return self._dryrun

    @dryrun.setter
    def dryrun(self, dryrun_input):
        self._dryrun = dryrun_input

    @property
    def response(self):
        return self._response

    @response.setter
    def response(self, response_input):
        self._response = response_input

    @property
    def iteration_buffer(self):
        return self._iteration_buffer

    @iteration_buffer.setter
    def iteration_buffer(self, iteration_buffer_input):
        self._iteration_buffer = iteration_buffer_input

    # functions used for the callable methodes
    def get_query_string(self):
        if self.query is None or self.query == '':
            self.logger.error('Query not specified')
        else:
            if isinstance(self.query, str):
                result = self.query
                if self.wq_max is not None:
                    result = "wq_max={}&{}".format(self.wq_max, result)
                if self.wq_ofs is not None:
                    result = "wq_ofs={}&{}".format(self.wq_ofs, result)
                return result
            elif isinstance(self.query, list):
                result = []
                if self.wq_ofs is not None:
                    result.append("wq_ofs={}".format(self.wq_ofs))
                if self.wq_max is not None:
                    result.append("wq_max={}".format(self.wq_max))
                # iterate key-value pairs
                for key in self.query.iter():
                    value = "" if key.text is None else key.text
                    # check for 'and not' exclamation mark before key
                    is_wqnot = key[:1] == "!"
                    if is_wqnot:
                        key = key[1]
                        result.append("wq_rel=AND+NOT")
            else:
                self.logger.error('Query should be string or list')

    def get_search_url(self):
        # combine host, service, suffix with WebQuery to one url
        # add suffix if relevant
        if self.suffix is None:
            url = "{}/WebQuery/{}".format(self.host, self.service)
        else:
            url = "{}/WebQuery/{}/{}".format(self.host, self.service, self.suffix)
        # search with isn
        if self.isn is not None:
            url += "/{}".format(self.isn)
        
        if self.query is not None:
            # formulate query from filters
            qry = self.get_query_string()
            # search with query
            if qry != '':
                url += "?{}".format(qry)

        return url

    def get_next_ofs(self, response):
        """
        Get the next record cursor if available
        """
        if (response is None):
            return False

        # try to filter wq_ofs and wq_max from next tag
        tree = ET.fromstring(response.content)
        try:
            self.wq_ofs = tree.xpath("next/@wq_ofs")[0]
            self.wq_max = tree.xpath("next/@wq_max")[0]
            # return success
            return True
        except:
            # The final record has been reached if there is no next tag. stop searching for new records
            self.logger.info("Final record reached, search ended")
            self.wq_ofs = None
            self.wq_max = None
            return False

    # Main methods of module

    def get_new_url(self):
        """Returns url for posting new records"""
        return "{}/WebQuery/{}/new_{}".format(self.host, self.service, self.suffix)

    def get_update_url(self):
        """Returns url for updating existing records"""
        return "{}/WebQuery/{}/update_{}/{}".format(self.host, self.service, self.suffix, self.isn)

    def search(self):
        """
        search request with set parameters
        stops when wq_max is reached
        :return: records till wq_max is reached
        """

        # raise error if service is empty
        if self.service is None or self.service == '':
            raise ValueError("search: service not set")

        try:
            the_url = self.get_search_url()
            self.logger.info("requesting: {}".format(the_url))
            # try to find a response
            response = requests.get(the_url, timeout=self.timeout)
            # raise for status code
            response.raise_for_status()
            return response

        # catch exceptions and log results
        except requests.exceptions.HTTPError as errh:
            self.logger.error("Http Error during search: {}".format(errh))
            raise IOError
        except requests.exceptions.ConnectionError as errc:
            self.logger.error("Error Connecting during search: {}".format(errc))
            raise IOError
        except requests.exceptions.Timeout as errt:
            self.logger.error("Timeout Error during search: {}".format(errt))
            raise IOError
        except requests.exceptions.RequestException as err:
            self.logger.error("Non Standard Error during search: {}".format(err))
            raise IOError

    def search_all_records(self):
        """
        run a search request with set request parameters
        continue searches and collect all records until no next element is found
        :return: all collected records from the search
        """
        # search start
        self.logger.info("search started: host: {}, service: {}, suffix: {}, query: {}"\
                          .format(self.host, self.service, self.suffix, self.query))

        # set start variables
        records = []
        ofs = True

        # as long as search has a next tag continue
        while ofs:
            # search with given parameters and attributes
            search = self.search()
            if not search:
                raise LookupError("search failed, stopping execution")
            # add all search responses to list
            records.append(search)
            # look for next wq_ofs
            ofs = self.get_next_ofs(search)

        return records


    # ITERATOR methods

    def __iter__(self):
        # initialize buffer. starts empty
        self.iteration_buffer = []
        # wq_ofs should not be None because that is the stop condition
        self.wq_ofs = 0
        return self

    def __next__(self):
        """
        Return the next record or raise StopIteration
        """
        # happy flow, we have a next record available
        if (len(self.iteration_buffer) > 0):
            # return first element from buffer
            return self.iteration_buffer.pop(0)

        if (self.wq_ofs is None):
            # zero records or no more records to retrieve
            raise StopIteration
        
        # fill buffer with next record set
        response = self.search()
        self.iteration_buffer = self.create_array_from_wq_records(response.content)
        # get wq_ofs if available
        self.get_next_ofs(response)

        if (len(self.iteration_buffer)== 0):
            # zero records has been retrieved
            raise StopIteration

        # return first element from buffer
        return self.iteration_buffer.pop(0)

    # END ITERATOR methods

    def create_array_from_wq_records(self, xmlstring):
        """
        Create an array from the records in the xmlstring as ETree elements
        """
        full_xml = ET.fromstring(xmlstring)
        # determine the name of the set element, so we can abstract the child elements from that
        # titelbeschrijving-set -> titelbeschrijving
        element_name = full_xml.tag[0:-4]
        response = full_xml.xpath('./{}'.format(element_name))
        return response

    def create_kv_pairs(self, xml):
        """Create string with key-value pairs based on input xml for posting to WebQuery
        Parameters
        ----------
        xml : str

        Returns
        -------
        result : str

        """
        result = []
        # convert string xml to xml tree object and remove comments
        xmldoc_root = ET.fromstring(xml, parser=ET.XMLParser(remove_comments=True))
        # iterate to create key-value pairs
        for key in xmldoc_root.iter():
            value = "" if key.text is None else key.text
            # urlencode and trim()
            self.logger.debug("parsing: {}".format(value.strip()))
            value = urllib.parse.quote_plus(value.strip(), ':/?=')
            result.append("{}={}".format(key.tag, value))

        root_element = result[0]
        # WebQuery expects the record name as part of the url. Skip the -set element and the record name element
        # example: <titel-set><titel><foo>bar</foo><baz>qux</baz></titel></titel-set>
        # will be returned as foo=bar&baz=qux
        if '-set=' in root_element:
            # Ignore record-set AND record elements
            # and return child elements of record element as key-value pairs, seperated with '&' character
            return '&'.join(result[2:])
        else:
            # Ignore record element
            # and return child elements of record element as key-value pairs, seperated with '&' character
            return '&'.join(result[1:])

    def create_record(self, xml):
        """formats input xml and posts it to WebQuery as a new record

        takes input xml, calls get_new_url() and create_kv_pairs() to build request 
        then calls post_xml() to send the body to the url

        Parameters
        ----------
        xml : str

        Returns
        -------
        post_xml() : int
        """
        if self.service is None or self.service == '':
            self.logger.error("create record: service is not set")
            return False
        else:
            self.logger.info("create record")
            url = self.get_new_url()
            body = self.create_kv_pairs(xml)
            return self.post_xml(url, body)

    def update_record(self, xml):
        """formats input xml and posts it to WebQuery as an update to a record

        takes input xml, calls get_new_url() and create_kv_pairs() to build request 
        then calls post_xml() to send the body to the url

        Parameters
        ----------
        xml : str

        Returns
        -------
        post_xml() : int
        """
        if self.service is None or self.service == '':
            self.logger.error("update record: service is not set")
            return False
        else:
            self.logger.info("update record")
            url = self.get_update_url()
            kv_pairs = self.create_kv_pairs(xml)
            body = 'wq_crc={}&{}'.format(self.crc, kv_pairs)
            return self.post_xml(url, body)

    def post_xml(self, url: str, body: str, method: str = 'post') -> str:
        """
        send xml through a http request
        :param url: target URL
        :param body: xml to be send as string
        :param method: http verb, defaults to post
        :return: statuscode
        """
        dryrun = self.dryrun

        if dryrun:
            self.logger.debug("DRYRUN: {}ing data to {}: {}".format(method, url, body))
            return '201'
        else:
            self.logger.debug("{}ing data to {}: {}".format(method, url, body))
            headers = {'Content-Type': "application/x-www-form-urlencoded", 'cache-control': "no-cache", 'charset': "utf-8"}
            response = send_request(url, method=method, data=body, headers=headers)

            data_response = ET.fromstring(response["responsedata"].encode('utf8'))
            self.data_response = data_response
            if data_response.find('error') is not None:
                if data_response.find('error').attrib['code'] == 'WQW_UPDATE_OK':
                    # happy flow
                    self.logger.info('change posted to {} for isn: {}'.format(self.service, self.isn))
                elif data_response.find('error').attrib['code'] == 'WQW_UPDATE_NOCHANGE':
                    self.logger.warning('no change in {} for isn: {}'.format(self.service, self.isn))
                elif data_response.find('error').attrib['code'] == 'WQW_XML_NOT_XSD_VALID':
                    self.logger.warning("send xml was not in accordance to XSD: {} for service: {} and suffix {}".format(body, self.service, self.suffix))
                else:
                    self.logger.warning('Encountered error for isn: {} with code {} stating {}'.format (
                        self.isn,
                        data_response.find('error').attrib['code'],
                        data_response.find('error/message').text)
                    )
            else:
                self.logger.error('unexpected response returned.')

            return response["statuscode"]
