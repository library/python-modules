#!/usr/bin/env python3
import requests
from tenacity import *
from typing import Dict

def send_request(url, method='get', data=None, headers=None, encoding='utf-8') -> Dict:
    """Sends url request using provided url.
    Tenacity will retry request on errors, up to 5 times.

    Parameters
    ----------
    url : str
        string with url to retrieve data from
    method : str
        request method to use, {'post', 'get'}} defaults to GET
    data : str
        data to send
    headers : Dict
    encoding : str

    Returns
    -------
    data_response : Dict
        {statuscode, message/data of response}

    """
    response = None
    try:
        if method == 'get':
            response = send_get_request(url, encoding)
        if method == 'post':
            response = send_post_request(url, data, headers)
        if response is not None:
            responsedata = response.text
            responsecode = response.status_code
            return {"statuscode": responsecode, "responsedata": responsedata}
        else:
            return {"statuscode": None, "responsedata": None}
    except Exception:
        raise


@retry(stop=stop_after_attempt(5))
def send_get_request(url, encoding):
    try:
        response = requests.get(url)
        response.encoding = encoding
        return response
    except Exception:
        # pass exception on
        raise


@retry(stop=stop_after_attempt(5))
def send_post_request(url, data, headers):
    try:
        response = requests.post(url, data=data, headers=headers)
        return response
    except Exception:
        # pass exception on
        raise
