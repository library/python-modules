import datetime
import dateutil.parser
import os
import sys
import logging
from os import path
sys.path.append('../')
from WurModules.util.sendMail import Mailer

class ExceptionTimer:
    """
    This class checks if an exception occurred less than :timeout minutes ago.
    It only sends an email to mdt.library if the exception persists longer than :timeout
    should be used in cases where the exception may be the result of a problem
    outside the application, and the application will pick up missed prcessing
    automatically if the external problem has been resolved. E.g. connection error
    Only one email will be send if the exception persists

    Parameters
    ----------
    tmp_path : str
         path to a writable directory to store temp files. usa. /tmp (default)
    """
    def __init__(self, tmp_path="/tmp", dry_run=False):
        self._logger = logging.getLogger(__name__)
        self._tmp_path = tmp_path
        self._dry_run = dry_run

    @property
    def dry_run(self):
        return self._dry_run

    @dry_run.setter
    def dry_run(self, toggle):
        self._dry_run = toggle

    @property
    def tmp_path(self):
        return self._tmp_path

    @tmp_path.setter
    def tmp_path(self, the_path):
        self._tmp_path = the_path

    def notify_exception(self, filename, exception_type, timeout, exit_process=True):
        """
        Checks if an exception occurred before this one
        If it did not: create file to take note of current datetime: first time the error occurred
        If it did, but no longer than :timeout minutes ago, ignore this exception
        If it occurred longer then :timout minutes: email warning to mdt.library

        Parameters
        ----------
        filename : str
            name of the file (or process) in which the error occured
        exception_type: str
            Identifies the occurred exception like "IOError"
        timeout : int
            numeric value as a string, number of minutes to wait before sending an e-mail
        exit_process : bool
            exit the process after an exception is registered, default: True
        """
        if path.exists("{}/{}.txt".format(self.tmp_path, exception_type)):
            exceptionFile = open('{}/{}.txt'.format(self.tmp_path, exception_type))
            lines = exceptionFile.readlines()
            exceptionFile.close()
            # read the second line which is the datetime of last occurrence
            dt = dateutil.parser.parse(lines[1])
            now_minus_n_minutes = datetime.datetime.now() - datetime.timedelta(minutes=timeout)
            if dt < now_minus_n_minutes:
                self._logger.info("Last time this happend is longer than {} minutes ago!".format(timeout))
                # has an email already been sent? i.e. do we have a third line?
                if len(lines) > 2:
                    # line 3 is something like: "an email about this has been sent at {date time}"
                    if self.dry_run:
                        print(lines[2])
                    else:
                        self._logger.info(lines[2])
                    if exit_process:
                        sys.exit(1)
                else:
                    self._logger.info('Sending email to notify about this error')
                    try:
                        if (not self.dry_run):
                            Mailer.send_mail(
                                recipient="mdt.library@wur.nl",
                                subject="Too many exceptions of type {}".format(exception_type),
                                body="Error in file {}. Too many request to WebQuery returned a 500 status."
                                    .format(filename)
                            )
                        # write 'email sent' to the exceptions file. prevents sending it again
                        f = open("{}/{}.txt".format(self.tmp_path, exception_type), "a")
                        f.write("Email about this error has been sent: {}\n"
                                .format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
                        f.close()
                    except Exception as e:
                        # this should trigger an error email as well - but this will probably fail as well...
                        print("Could not send email to warn about IOError!")
                    if exit_process:
                        sys.exit(1)
            else:
                self._logger.info("Last time this error happened is less than {} minutes ago. Exiting silently."
                                  .format(exception_type))
                if exit_process:
                    sys.exit(1)
        else:
            # first occurrence: create file
            currentDT = datetime.datetime.now()
            f = open("{}/{}.txt".format(self.tmp_path, exception_type), "w")
            f.writelines(["IOError occurred at\n", "{}\n".format(currentDT.strftime("%Y-%m-%d %H:%M:%S"))])
            f.close()

    def reset_notify_exception(self, exception_type):
        """
        Removes an exception file of a given type if present

        Parameters
        ----------
        exception_type : str
            Identifies the occurred exception like "IOError".
        """
        if path.exists("{}/{}.txt".format(self.tmp_path, exception_type)):
            self._logger.info("removing exception file type {} from {}".format(exception_type, self.tmp_path))
            os.remove("{}/{}.txt".format(self.tmp_path, exception_type))