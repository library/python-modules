import json
import operator

class GetConfig:
    """Used for loading a config json, and retrieving values from this config.

    This class will parse a json config file. On instantiating, a path toe the file must be provided. 
    Optionally, an environment can be provided, this defaults to DEFAULT if none is provided.
    If a key is not found in the environment, get_config_value() will look in DEFAULT as well.

    Usage
    -----
    This class allows you to load a default config file by instantiating the class.
    Values can be retrieved using get_config_value(), sub-elements can be retrieved via dot notation, 
    which will be unpacked with the find() helper function.
    selective_merge allows for the default config to be overwritten/appended by a local config file. 
    The path for the local config file should be added to the default config

    """
    def __init__(self, config_path, config_env = "DEFAULT"):
        self._config_env = config_env
        self._config_path = config_path

        self.load_config()

    def load_config(self):
        """
        Automatically called on instantiation.
        Loads config file. if a LOCAL_CONFIG is provided, this will overwrite/append the config file.
        """
        try:
            with open(self._config_path) as config_file:
                self._config = json.load(config_file)
        except Exception as e:
            print ('No config file found')
            print (e)
            exit(1)
        try:
            local_config = self.get_config_value('LOCAL_CONFIG')
            with open(local_config) as local_config_file:
                local_config = json.load(local_config_file)
                self._config = self.selective_merge(self._config, local_config)
        except (IOError, KeyError):
            pass
            #print ('no local config or path to local config found')
        #return config


    def selective_merge(self, base_obj, delta_obj):
        """
        Helper function to overwite/append with local config file
        """
        if not isinstance(base_obj, dict):
            return delta_obj
        common_keys = set(base_obj).intersection(delta_obj)
        new_keys = set(delta_obj).difference(common_keys)
        for k in common_keys:
            base_obj[k] = self.selective_merge(base_obj[k], delta_obj[k])
        for k in new_keys:
            base_obj[k] = delta_obj[k]
        return base_obj

    def find(self, element, json):
        """
        Helper function to parse dot notation in get_config_value input
        """
        keys = element.split('.')
        rv = json
        for key in keys:
            rv = rv[key]
        return rv

    def get_config_value(self, constant_name):
        """
        Returns config value based on provided constant_name. dot notation can be used for nested values.

        Parameters
        ----------
        constant_name : str
            name of key for value in config file. Dot notation can be used for nested values.

        Returns
        -------
        constant_value : str
            value matching the provided key.        
        """
        try:
            constant_value = self.find(self._config_env+'.'+ constant_name, self._config)
        except:
            constant_value =  self.find('DEFAULT.'+constant_name, self._config)

        return constant_value
