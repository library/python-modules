#!/usr/bin/env python3
import os
import logging

class Lastrun:
    """Used to get and set lastrun time, this can be used for retrieving changes 
    since lastrun time, and for timestamp in filename

    Usage:
    at start of script, store return value from get_lastrun().
    At end of script, set new lastrun. This should be the start of script time, 
    to ensure no changes posted during script runtime are lost.
    """
    def __init__(self, lastrun_file):
        self._lastrun_file = lastrun_file
        
        self._logger=logging.getLogger(__name__)

    def set_lastrun(self, current_start_time):
        """Set lastrun date

        Parameters
        ----------
        current_start_time : str
            start time of script

        Returns
        -------
        void
        """
        try:
            with open(self._lastrun_file, 'w') as text_file:
                text_file.write(current_start_time)
        except:
            self._logger.error("Cannot save lastrun time.")

    def get_lastrun(self):
        """Set lastrun date

        Returns
        -------
        lastrun_date : string
            string with content of lastrun_fi
        """
        lastrun_date = None

        if os.path.isfile(self._lastrun_file):
            f = open(self._lastrun_file, 'r')
            lastrun_date = f.read().strip()
        else:
            self._logger.error('No lastrun time found')
            exit(1)

        return lastrun_date
