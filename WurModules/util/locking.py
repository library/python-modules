#!/usr/bin/env python3
import os

class Lock:
    """Used for creating and releasing a lock using a lockfile

    The main object of the locking mechanism is to prevent two processes to run at once.
    To this end, a lock file is created containing the process-id of the process that created it.
    When a subsequent process tries to set a lock, the set_lock method looks for the lock file,
    and, if it exists, checks if the registered process (id) is still running.
    If so, it returns False indicating that the new process should not be started.

    Usage
    -----
    This class has two methods: set_lock() and remove_lock().
    Instantiate a lock object passing a lockfile (relative path) to use.
    use set_lock() at the beginning of the process and remove_lock() at the end.

    """

    def __init__(self, lock_file: str) -> None:
        """
        Instantiate and set name of lockfile (mandatory)
        :param lock_file: 
        """
        self._lock_file = lock_file

    def set_lock(self) -> bool:
        """
        Try to set the lockfile
        :return: bool
        """
        pid = os.getpid()

        if os.path.isfile(self._lock_file):
            # lock_file exists
            # check if and how ong the process is running
            with open(self._lock_file, 'r') as lock_file_f:
                old_pid = lock_file_f.read().strip()

            if os.path.exists('/proc/{}'.format(old_pid)):
                # process still running
                ret_val = False
            else:
                # process not running. ignore lock
                ret_val = True
        else:
            # success, lock file set
            ret_val = True

        if ret_val:
            # set current PID in the lock file
            with open(self._lock_file, 'w') as text_file:
                text_file.write(str(pid))

        return ret_val

    def remove_lock(self) -> None:
        """
        Remove the lockfile
        :return: void 
        """

        if os.path.isfile(self._lock_file):
            os.remove(self._lock_file)

    def is_locked(self) -> bool:
        """
        Try to set the lockfile
        :return: bool
        """
        ret_val = False
        if os.path.isfile(self._lock_file):
            # lock_file exists
            # check if and how long the process is running
            with open(self._lock_file, 'r') as lock_file_f:
                old_pid = lock_file_f.read().strip()
            # check if PID exists (still runnuing)
            ret_val = os.path.exists('/proc/{}'.format(old_pid))

        return ret_val
