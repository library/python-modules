#!/usr/bin/env python3
import hashlib
import sys

class Sha1:

    def __init__(self, path):
        self._path_to_file = path

    def getSha1(self):
        '''
        Calculates the sha1 sum for the file referenced in path_to_file
        :return: sha1 string
        '''
        with open(self._path_to_file, 'rb') as f:
            contents = f.read()
            return hashlib.sha1(contents).hexdigest()

def main():
    if len(sys.argv) != 3:
        print("Use shacalc.py fullPathToFile format")
    else:
        filepath = sys.argv[1]
        format = sys.argv[2]
        if format == "sha1":
            s = Sha1(filepath)
            print(s.getSha1())


if __name__ == "__main__": main()

