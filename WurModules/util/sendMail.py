#!/usr/bin/env python3
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from subprocess import Popen, PIPE

class Mailer:
    @classmethod
    def send_email(self, to, fromLabel, body, subject):
        """Send e-mail with provided arguments. Uses linux's built in sendmail.
        -- DEPRECATED - Use send_mail() --
        Parameters
        ----------
        to : str
            e-mail adress of recipient
        fromLabel : str
            name of sender
        body : str
            content of e-mail
        subject : str
            subject of e-mail

        Returns
        -------
        void
        """
        msg = MIMEText(body)
        msg["From"] = fromLabel
        msg["To"] = to
        msg["Subject"] = subject
        p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE, universal_newlines=True)
        p.communicate(msg.as_string())

    @classmethod
    def send_mail(self, recipient, body, subject, sender='noreply.library@wur.nl', reply_to='', assume_body_html=False):
        """
        Send email using Linux's sendmail command
        Wraps the body into <html> tags, if not already present
        Replaces \n with <br/> if present
        :return: None
        """
        body_html = body.replace('\n', '<br/>')
        if body_html.find("<html>") == -1:
            html = MIMEText("<html><head><title>{}</title></head><body>{}</body>".format(subject, body_html), "html")
        else:
            if assume_body_html:
                html = MIMEText(body_html, "html")
            else:
                html = MIMEText(body_html)

        msg = MIMEMultipart("alternative")

        msg["From"] = sender
        msg["Reply-to"] = reply_to
        msg["To"] = recipient
        msg["Subject"] = subject

        msg.attach(html)

        p = Popen(["/usr/sbin/sendmail", "-t"], stdin=PIPE)
        p.communicate(msg.as_string().encode('UTF-8'))
