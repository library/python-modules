import unittest
import sys
from os import path
from contextlib import redirect_stdout
import io

# because tests is in a subdir we need to look 'up' for the module dir
sys.path.append('../')

from WurModules.util.exceptionTimer import ExceptionTimer

class ExceptionTimerTester(unittest.TestCase):

    def test_create_exceptions_file(self):
        st = ExceptionTimer()
        # cleanup previous run
        st.reset_notify_exception(exception_type="IOError")
        # create file, ignore this exception for 5 minutes
        exit_proces = False
        st.notify_exception("tester", "IOError", 5, exit_proces)
        self.assertTrue(path.exists('/tmp/IOError.txt'))
        # should now contain two lines:
        exceptionFile = open('/tmp/IOError.txt')
        lines = exceptionFile.readlines()
        self.assertEqual(len(lines), 2)
        # cleanup
        exceptionFile.close()
        st.reset_notify_exception(exception_type="IOError")
        self.assertFalse(path.exists('/tmp/IOError.txt'))

    def test_send_email_after_timeout(self):
        st = ExceptionTimer()
        # cleanup previous run
        st.reset_notify_exception(exception_type="IOError")
        # setup a file, set time of occurrence > 5 minutes
        new_contents = ['IOError occurred at\n', '2020-01-01 23:59:00\n']
        # and write everything back
        with open('/tmp/IOError.txt', 'w') as file:
            file.writelines(new_contents)

        exit_proces = False
        # it should now attempt to send an email
        # this wont succeed, so I'll test for the email error message
        # this message will be send to stdout to trigger an email
        f = io.StringIO()
        with redirect_stdout(f):
            st.notify_exception("tester", "IOError", 5, exit_proces)
        self.assertEqual(f.getvalue(), "Could not send email to warn about IOError!\n")
        f.close()

        # set dry_run
        st_dryrun = ExceptionTimer(dry_run=True)
        st_dryrun.notify_exception("tester", "IOError", 5, exit_proces)
        # now the mail should be sent and noted in the exceptions file
        exceptionFile = open('/tmp/IOError.txt')
        lines = exceptionFile.readlines()
        exceptionFile.close()
        self.assertEqual(len(lines), 3)
        # the next time, an email will not be sent, but process is stopped silently
        # logging is redirected to std_out because of dry run. so we can test it
        f = io.StringIO()
        with redirect_stdout(f):
            st_dryrun.notify_exception("tester", "IOError", 5, exit_proces)
        resp = f.getvalue()
        self.assertEqual(resp[0:36], "Email about this error has been sent")
        f.close()
