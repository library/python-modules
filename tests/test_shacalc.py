import unittest
import sys
# because tests is in a subdir we need to look 'up' for the module dir
sys.path.append('../')

from WurModules.util.shacalc import Sha1


class Sha1Tester(unittest.TestCase):
    def test_sha1(self):
        sc = Sha1('tests/testdocs/edepotlink_i146_001.pdf')
        self.assertEqual(sc.getSha1(), 'b21f1addf42ddc4ff3926684d2cd0e36a892bdd2')

    def test_sha1_filenotfound(self):
        Sha1('tests/testdocs/notafile.pdf')
        self.assertRaises(FileNotFoundError)


if __name__ == '__main__':
    unittest.main()
