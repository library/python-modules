import unittest
import sys
from unittest.mock import patch

# because tests is in a subdir we need to look 'up' for the module dir
sys.path.append('../')

from WurModules.webquery.webquery import WQReceiver

class WQTester(unittest.TestCase):

    def test_properties(self):
        wq = WQReceiver()

        self.assertEqual(wq.host, "https://library.wur.nl", "default host")
        self.assertEqual(wq.suffix, "xml", "default suffix")
        self.assertEqual(wq.timeout, 60, "default timeout")

        wq = WQReceiver()
        wq.host = "https://devel.library.wur.nl"
        wq.service = "any"
        self.assertEqual(wq.host, "https://devel.library.wur.nl", "set host")
        self.assertEqual(wq.service, "any", "set service")
        self.assertEqual(wq.suffix, "xml", "default suffix")

        wq = WQReceiver()
        new_host = "mynewhost"
        wq.host = new_host
        self.assertEqual(new_host, wq.host, "set/get attribute host")

        new_service = "newservice"
        wq.service = new_service
        self.assertEqual(new_service, wq.service, "set/get attribute service")

        new_suffix = "newsuffix"
        wq.suffix = new_suffix
        self.assertEqual(new_suffix, wq.suffix, "set/get attribute suffix")
        
        new_timeout = 30
        wq.timeout = new_timeout
        self.assertEqual(new_timeout, wq.timeout, "set/get attribute timeout")

        self.assertFalse(wq.dryrun, "is dryrun mode, dryrun not set")
        value = True
        wq.dryrun = value
        self.assertEqual(value, wq.dryrun, "set/get attribute dryrun")
        self.assertTrue(wq.dryrun, "is dryrun mode, dryrun is set")

    def test_methods(self):
        wq = WQReceiver()
        wq.service = "edepotbeheer"
        wq.query = "q=*"
        url = "https://library.wur.nl/WebQuery/edepotbeheer/xml?q=*"
        self.assertEqual(url, wq.get_search_url(), "get search url with parameters")

        wq = WQReceiver()
        wq.service = "edepotbeheer"
        wq.suffix = "edit/new"
        wq.query = "wq_inf_edit=bestand"
        url = "https://library.wur.nl/WebQuery/edepotbeheer/edit/new?wq_inf_edit=bestand"
        self.assertEqual(url, wq.get_search_url(), "get search url with parameters (changed suffix)")

        wq = WQReceiver()
        wq.service = "edepotbeheer"
        wq.isn = 10
        url = "https://library.wur.nl/WebQuery/edepotbeheer/xml/10"
        self.assertEqual(url, wq.get_search_url(), "get search url with isn")

        wq = WQReceiver()
        wq.service = "edepotbeheer"
        wq.isn = 10
        wq.query = "wq_dbg=on"
        url = "https://library.wur.nl/WebQuery/edepotbeheer/xml/10?wq_dbg=on"
        self.assertEqual(url, wq.get_search_url(), "get search url with isn and query parameters")

        wq = WQReceiver()
        wq.service = "edepotbeheer"
        url = "https://library.wur.nl/WebQuery/edepotbeheer/new_xml"
        self.assertEqual(url, wq.get_new_url(), "get new url")

        url = "https://library.wur.nl/WebQuery/edepotbeheer/update_xml"
        self.assertNotEqual(url, wq.get_update_url(), "get update url (isn not set)")
        wq.isn = 20
        url = "https://library.wur.nl/WebQuery/edepotbeheer/update_xml/20"
        self.assertEqual(url, wq.get_update_url(), "get update url (with isn)")

        wq = WQReceiver()
        self.assertEqual(None, wq.get_query_string(), "get query string (query not set)")
        wq.query = ''
        self.assertEqual(None, wq.get_query_string(), "get query string (query is an empty string)")

        """ #Niet duidelijk hoe de test moet zijn of dat de methode aangepast moet worden
        wq = WQReceiver()
        wq.query = "q=*&wq_max=0"
        self.assertEqual(None, wq.search(), "search (without service!)") """

        wq = WQReceiver()
        xml='<lfl-set><lfl isn="8166" crc="aece5910"><id>l4l:education:2295114</id><status>ok</status></lfl></lfl-set>'
        key_value_pairs="id=l4l:education:2295114&status=ok"
        self.assertEqual(key_value_pairs, wq.create_kv_pairs(xml), "create_kv_pairs - with recordset")
        xml='<lfl isn="8166" crc="aece5910"><id>l4l:education:2295114</id><status>ok</status></lfl>'
        self.assertEqual(key_value_pairs, wq.create_kv_pairs(xml), "create_kv_pairs - without recordset")

        wq = WQReceiver()
        wq.dryrun = True
        url = "https://backend.library.wurnet.nl/WebQuery/l4l/update_xml-oracle/8166"
        body="wq_crc=aece5910&id=l4l:education:2295114&status=ok"
        self.assertEqual('201', wq.post_xml(url, body), "post_xml - dryrun is True")

        wq = WQReceiver()
        xml = "<root><a><b/></a></root>"
        self.assertEqual(False, wq.create_record(xml), "create_record (without service!)")
        self.assertEqual(False, wq.update_record(xml), "update_record (without service!)")

    @patch('requests.get')
    def test_wq_single_call(self, mock_get):
        """
        Test a single request using "search"
        """
        host = "https://library.wur.nl"
        query = "titelbeschrijving/@hd=2019-01-01T00:00:00Z//2021-03-03T17:47:56Z"
        test_url = "https://library.wur.nl/WebQuery/l4lbron/xml?{}".format(query)
        
        mock_get.return_value.content = "<response />"
        mock_get.return_value.status_code = 200

        wq = WQReceiver(
            host=host,
            service="l4lbron",
            suffix="xml",
            query=query
        )

        result = wq.search()
        mock_get.assert_called_once_with(test_url, timeout=60)
        # test assembling the parts of the URL
        the_search_url = wq.get_search_url()
        self.assertEqual(the_search_url, test_url)
        # test search has
        self.assertEqual(result.content, "<response />")

    @patch('requests.get')
    def test_iterator(self, mock_get):
        host = "https://library.wur.nl"
        query = "titelbeschrijving/@hd=2019-01-01T00:00:00Z//2021-03-03T17:47:56Z&wq_max=2"
        test_url = "https://library.wur.nl/WebQuery/l4lbron/xml?{}".format(query)

        # get set of two records, prepare wq_ofs for next 2 reords
        mock_get.return_value.content \
            = '<record-set><record>a</record><record>b</record><next wq_ofs="2" wq_max="2"/></record-set>'
        mock_get.return_value.status_code = 200

        wq = WQReceiver(
            host=host,
            service="l4lbron",
            suffix="xml",
            query=query
        )

        self.assertEqual(wq.get_search_url(), test_url)
        self.assertEqual(wq.response, None)

        first_run = True
        total_nr_of_records = 0
        last_record = None
        # start iteration, until no more records
        for record in wq:
            total_nr_of_records += 1
            last_record = record # hang on to the last received element
            if first_run:
                self.assertTrue(wq.wq_ofs == '2')
                self.assertTrue(wq.wq_max == '2')
                self.assertEqual(record.text, 'a', "correct order of emptying the record buffer")
                # update the mock response: no more next element, next will be the last record-set
                mock_get.return_value.content = '<record-set><record>c</record></record-set>'
                first_run = False
                self.assertTrue(len(wq.iteration_buffer) == 1)

        # during the iteration, 3 records have been retrieved. The iterator needed 2 requests for that
        self.assertEqual(total_nr_of_records, 3, "retrieved 3 records")
        self.assertTrue(mock_get.call_count == 2)
        self.assertEqual(last_record.text, 'c')

    @patch('requests.get')
    def test_iterator_zero_records(self, mock_get):
        """
        Test for zero records - WQW_NO_HITS
        """
        host = "https://library.wur.nl"
        query = "wq_ofs=0&titelbeschrijving/@hd=2021-07-15T02:33:31Z//2021-07-21T10:41:07Z"
        test_url = "https://library.wur.nl/WebQuery/l4lbron/xml?{}".format(query)

        # get set of zero records
        mock_get.return_value.content \
            = '<record-set><error code="WQW_NO_HITS"><message>No records found that satisfy your query. Reformulate your query</message></error></record-set>'
        mock_get.return_value.status_code = 200

        wq = WQReceiver(
            host=host,
            service="l4lbron",
            suffix="xml",
            query=query
        )

        self.assertEqual(wq.get_search_url(), test_url)
        self.assertEqual(wq.response, None)

        total_nr_of_records = 0

        # start iteration, until no more records
        for record in wq:
            total_nr_of_records += 1

        self.assertEqual(total_nr_of_records, 0, "retrieved 0 records")

if __name__ == '__main__':
    unittest.main()